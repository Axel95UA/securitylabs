package com.proj.andoid.databindingtest.constants;

/**
 * created by Alex Ivanov on 2/21/16.
 */
public interface AppActions {

    String ACTION_LOGIN_COMPLETE = "com.proj.andoid.databindingtest.constants.ACTION_LOGIN_COMPLETE";
    String ACTION_DIR_SELECTED = "com.proj.andoid.databindingtest.constants.ACTION_DIR_SELECTED";

}
