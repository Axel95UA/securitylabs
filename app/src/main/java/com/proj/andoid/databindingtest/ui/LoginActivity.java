package com.proj.andoid.databindingtest.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.proj.andoid.databindingtest.R;
import com.proj.andoid.databindingtest.SecureApp;
import com.proj.andoid.databindingtest.UserPrefs;
import com.proj.andoid.databindingtest.constants.AppActions;
import com.proj.andoid.databindingtest.constants.AppKeys;
import com.proj.andoid.databindingtest.model.User;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * created by Alex Ivanov on 2/21/16.
 */
public class LoginActivity extends AppCompatActivity implements AppKeys {

    @Bind(R.id.emailEditText)
    protected EditText emailEditText;
    @Bind(R.id.passwordEditText)
    protected EditText passwordEditText;
    @Bind(R.id.loadingProgress)
    protected View loadingProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            emailEditText.setText(savedInstanceState.getString(KEY_EXTRAS1, ""));
            passwordEditText.setText(savedInstanceState.getString(KEY_EXTRAS2, ""));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_EXTRAS1, emailEditText.getText().toString());
        outState.putString(KEY_EXTRAS2, passwordEditText.getText().toString());
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            registerReceiver(onLoginComplete, new IntentFilter(AppActions.ACTION_LOGIN_COMPLETE));
        } catch (Exception ignore) {
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(onLoginComplete);
        } catch (Exception ignore) {
        }
    }

    private BroadcastReceiver onLoginComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            UserPrefs prefs = SecureApp.getApp().getUserPrefs();
            prefs.setUserProgile(user);
            prefs.setLogined();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    };
    private User user;

    @OnClick(R.id.loginButton)
    @SuppressWarnings("unused")
    protected void onLoginClick() {
        loadingProgress.setVisibility(View.VISIBLE);
        final String email = emailEditText.getText().toString();
        final String password = passwordEditText.getText().toString();
        new AsyncTask<Void, Void, User>() {
            @Override
            protected User doInBackground(Void... params) {
                List<User> userList = SecureApp.getApp().getDbInterface().getUsers();
                for (User user : userList) {
                    if (TextUtils.equals(user.getUserEmail(), email)) {
                        return user;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(User user) {
                super.onPostExecute(user);
                loadingProgress.setVisibility(View.GONE);
                if (user == null) {
                    emailEditText.setError(getString(R.string.error_email));
                    return;
                }
                if (!TextUtils.equals(user.getPassword(), password)) {
                    passwordEditText.setError(getString(R.string.error_password));
                    return;
                }
                LoginActivity.this.user = user;
                DialogFragment dialogFragment = new CapchaDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), "CAPCHA_DIALOG");
            }

        }.execute();
    }
}
