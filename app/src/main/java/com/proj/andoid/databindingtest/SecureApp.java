package com.proj.andoid.databindingtest;

import android.support.multidex.MultiDexApplication;

import com.proj.andoid.databindingtest.db.DbInterface;
import com.proj.andoid.databindingtest.model.User;

/**
 * created by Alex Ivanov on 2/16/16.
 */
public class SecureApp extends MultiDexApplication {

    private static SecureApp instance;
    private DbInterface dbInterface;
    private UserPrefs userPrefs;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        dbInterface = DbInterface.getInstance();
        dbInterface.insertUser(User.generateAdmin());
        userPrefs = UserPrefs.getInstance();
    }

    public static SecureApp getApp() {
        return instance;
    }

    public DbInterface getDbInterface() {
        return dbInterface;
    }

    public UserPrefs getUserPrefs() {
        return userPrefs;
    }
}
