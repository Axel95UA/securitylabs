package com.proj.andoid.databindingtest.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.proj.andoid.databindingtest.R;
import com.proj.andoid.databindingtest.SecureApp;
import com.proj.andoid.databindingtest.constants.AppActions;
import com.proj.andoid.databindingtest.constants.AppKeys;
import com.proj.andoid.databindingtest.model.User;
import com.proj.andoid.databindingtest.viewholder.RegistrationSelectHolder;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * created by Alex Ivanov on 2/22/16.
 */
public class RegistrationActivity extends AppCompatActivity {

    @Bind(R.id.registerToolbar)
    protected Toolbar toolbar;
    @Bind(R.id.emailEditText)
    protected EditText emailEditText;
    @Bind(R.id.passwordEditText)
    protected EditText passwordEditText;
    @Bind(R.id.registerRecyclerView)
    protected RecyclerView recyclerView;

    private ArrayList<String> selected = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            emailEditText.setText(savedInstanceState.getString(AppKeys.KEY_EXTRAS1, ""));
            passwordEditText.setText(savedInstanceState.getString(AppKeys.KEY_EXTRAS2, ""));
            selected = savedInstanceState.getStringArrayList(AppKeys.KEY_EXTRAS3);
            if (selected == null) {
                selected = new ArrayList<>();
            }
        }

        setSupportActionBar(toolbar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new RegistrationAdapter());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(AppKeys.KEY_EXTRAS1, emailEditText.getText().toString());
        outState.putString(AppKeys.KEY_EXTRAS2, passwordEditText.getText().toString());
        outState.putStringArrayList(AppKeys.KEY_EXTRAS3, selected);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_done:
                String email = emailEditText.getText().toString().trim();
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    emailEditText.setText(getString(R.string.error_registration_email));
                    return false;
                }
                String password = passwordEditText.getText().toString().trim();
                if (password.length() != 8) {
                    passwordEditText.setError(getString(R.string.error_password_length));
                    return false;
                }
                if (selected.size() == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(R.string.error_selected_dirs);
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.create().show();
                    return false;
                }
                User user = new User();
                user.setUserEmail(email);
                user.setPassword(password);
                user.setEnabledPath(selected);
                SecureApp.getApp().getDbInterface().insertUser(user);
                Toast.makeText(this, R.string.label_user_created, Toast.LENGTH_SHORT).show();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            registerReceiver(onDirChecked, new IntentFilter(AppActions.ACTION_DIR_SELECTED));
        } catch (Exception ignore) {
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(onDirChecked);
        } catch (Exception ignore) {
        }
    }

    private BroadcastReceiver onDirChecked = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String name = intent.getStringExtra(AppKeys.KEY_EXTRAS1);
            if (TextUtils.isEmpty(name)) {
                return;
            }
            if (selected.contains(name)) {
                selected.remove(name);
            } else {
                selected.add(name);
            }
        }
    };

    private class RegistrationAdapter extends RecyclerView.Adapter<RegistrationSelectHolder> {

        private LayoutInflater inflater;

        public RegistrationAdapter() {
            inflater = LayoutInflater.from(RegistrationActivity.this);
        }

        @Override
        public RegistrationSelectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new RegistrationSelectHolder(inflater.inflate(R.layout.listitem_register_dir, parent, false));
        }

        @Override
        public void onBindViewHolder(RegistrationSelectHolder holder, int position) {
            String name = String.valueOf(position + 1);
            holder.init(name, selected.contains(name));
        }

        @Override
        public int getItemCount() {
            return 40;
        }
    }
}
