package com.proj.andoid.databindingtest.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.proj.andoid.databindingtest.R;
import com.proj.andoid.databindingtest.SecureApp;
import com.proj.andoid.databindingtest.UserPrefs;
import com.proj.andoid.databindingtest.constants.AppActions;
import com.proj.andoid.databindingtest.constants.AppKeys;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * created by Alex Ivanov on 2/21/16.
 */
public class CapchaDialogFragment extends DialogFragment {

    @Bind(R.id.equation)
    protected TextView equation;
    @Bind(R.id.result)
    protected EditText result;

    private int resultInt;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(android.app.DialogFragment.STYLE_NO_TITLE);
        boolean isCancelable = getArguments().getBoolean(AppKeys.KEY_EXTRAS1, true);
        setCancelable(isCancelable);
        dialog.setCanceledOnTouchOutside(isCancelable);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialogfragment_capcha, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        generateCapcha();
    }

    private void generateCapcha() {
        int one = (int) (Math.random() * 5);
        ++one;
        int two = (int) (Math.random() * 12);
        ++two;
        equation.setText(getString(R.string.label_template, one, two));
        resultInt = one * one + two;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private int wrongCounter = 0;

    @OnClick(R.id.checkButton)
    @SuppressWarnings("unused")
    protected void onCheckButtonClick() {
        try {
            Integer solve = Integer.parseInt(result.getText().toString().trim());
            if (solve == resultInt) {
                getActivity().sendBroadcast(new Intent(AppActions.ACTION_LOGIN_COMPLETE));
                dismiss();
            } else {
                wrongCapcha();
            }
        } catch (Exception e) {
            wrongCapcha();
        }
    }

    private void wrongCapcha() {
        //TODO logs
        wrongCounter++;
        if (wrongCounter == 5) {
            SecureApp app = SecureApp.getApp();
            UserPrefs prefs = app.getUserPrefs();
            if (!TextUtils.equals(prefs.getUserProfile().getUserEmail(), "admin@super.me")) {
                SecureApp.getApp().getDbInterface().removeUser(prefs.getUserProfile().getId());
                Toast.makeText(getActivity(), R.string.label_profile_deleted, Toast.LENGTH_SHORT).show();
            }
            prefs.resetPrefs();
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        }
        result.setError(getString(R.string.error_capcha));
        generateCapcha();
    }
}
