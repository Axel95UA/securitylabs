package com.proj.andoid.databindingtest.viewholder;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.proj.andoid.databindingtest.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * created by Alex Ivanov on 2/21/16.
 */
public class DirectoryHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.dirName)
    protected TextView dirName;
    @Bind(R.id.dirInfo)
    protected View dirInfo;

    private String name;
    private int size;

    public DirectoryHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void init(String name, int size, boolean permission) {
        dirInfo.setVisibility(permission ? View.VISIBLE : View.GONE);
        this.name = name;
        this.size = size;
        dirName.setText(name);
    }

    @OnClick(R.id.dirInfo)
    @SuppressWarnings("unused")
    protected void onDirInfoClick() {
        Context c = itemView.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage(c.getString(R.string.label_dir_info, name, size));
        builder.setPositiveButton(android.R.string.ok, null);
        builder.create().show();
    }
}
