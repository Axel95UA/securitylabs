package com.proj.andoid.databindingtest.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.proj.andoid.databindingtest.SecureApp;
import com.proj.andoid.databindingtest.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * created by Alex Ivanov on 2/16/16.
 */
public class DbInterface {

    public static DbInterface singleton;
    private DbHelper helper;

    public static synchronized DbInterface getInstance() {
        if (singleton == null) {
            singleton = new DbInterface();
        }
        return singleton;
    }

    public DbInterface() {
        helper = new DbHelper(SecureApp.getApp());
    }

    public long insertUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(DbContract.UserFields.email, user.getUserEmail());
        cv.put(DbContract.UserFields.password, user.getPassword());
        cv.put(DbContract.UserFields.enabledPath, user.getPathAsString());
        return helper.getWritableDatabase().insert(DbHelper.Tables.USERS, null, cv);
    }

    public void removeUser(int id) {
        helper.getWritableDatabase().delete(DbHelper.Tables.USERS, BaseColumns._ID + "=" + id, null);
    }

    public List<User> getUsers() {
        List<User> userList = new ArrayList<>();
        Cursor cursor = helper.getReadableDatabase().query(DbHelper.Tables.USERS, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                String email = cursor.getString(cursor.getColumnIndex(DbContract.UserFields.email));
                String password = cursor.getString(cursor.getColumnIndex(DbContract.UserFields.password));
                String paths = cursor.getString(cursor.getColumnIndex(DbContract.UserFields.enabledPath));
                int id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
                    continue;
                }
                user.setUserEmail(email);
                user.setPassword(password);
                user.setId(id);
                user.parsePathFromString(paths);
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return userList;
    }

    public void insertLog(String email, String info) {
        ContentValues cv = new ContentValues();
        cv.put(DbContract.LogFields.userEmail, email);
        cv.put(DbContract.LogFields.info, info);
        cv.put(DbContract.LogFields.timestamp, System.currentTimeMillis());
        helper.getWritableDatabase().insert(DbHelper.)
    }
}
