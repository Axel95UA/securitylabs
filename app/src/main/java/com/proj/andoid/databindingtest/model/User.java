package com.proj.andoid.databindingtest.model;

import android.databinding.BaseObservable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * created by Alex Ivanov on 2/14/16.
 */
public class User extends BaseObservable {

    private String userEmail;
    private String password;
    private List<String> enabledPath;
    private int id;

    public User() {
        enabledPath = new ArrayList<>();
    }

    //SETTERS
    public void setEnabledPath(List<String> enabledPath) {
        this.enabledPath = enabledPath;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void parsePathFromString(String path) {
        String[] strArr = path.split("; ");
        for (String str : strArr) {
            if (!TextUtils.isEmpty(str)) {
                enabledPath.add(str);
            }
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    //GETTERS
    public List<String> getEnabledPath() {
        return enabledPath;
    }

    public String getPassword() {
        return password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getPathAsString() {
        String pathStr = "";
        for (String str : enabledPath) {
            pathStr += str + "; ";
        }
        return pathStr;
    }

    public int getId() {
        return id;
    }

    public static User generateAdmin() {
        User admin = new User();
        admin.setId(0);
        admin.setUserEmail("admin@super.me");
        admin.setPassword("adminRulerOfThis");
        admin.setEnabledPath(new ArrayList<String>());
        return admin;
    }
}
