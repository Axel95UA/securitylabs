package com.proj.andoid.databindingtest.constants;

/**
 * created by Alex Ivanov on 2/21/16.
 */
public interface AppKeys {

    String KEY_EXTRAS1 = "key.extras.1";
    String KEY_EXTRAS2 = "key.extras.2";
    String KEY_EXTRAS3 = "key.extras.3";
    String KEY_USER = "key.user";

}
