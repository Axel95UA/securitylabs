package com.proj.andoid.databindingtest.db;

import android.provider.BaseColumns;

import com.proj.andoid.databindingtest.BuildConfig;

/**
 * created by Alex Ivanov on 2/15/16.
 */
public class DbContract {

    public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".DataProvider";

    public interface UserFields extends BaseColumns {
        String email = "email";
        String password = "password";
        String enabledPath = "enabledPath";
    }

    public interface LogFields extends BaseColumns {
        String userEmail = "userEmail";
        String info = "info";
        String timestamp = "timestamp";
    }
}
