package com.proj.andoid.databindingtest.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.proj.andoid.databindingtest.R;
import com.proj.andoid.databindingtest.SecureApp;
import com.proj.andoid.databindingtest.UserPrefs;
import com.proj.andoid.databindingtest.constants.AppKeys;
import com.proj.andoid.databindingtest.viewholder.DirectoryHolder;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.mainToolbar)
    protected Toolbar toolbar;
    @Bind(R.id.mainRecycler)
    protected RecyclerView mainRecyclerView;

    private UserPrefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = SecureApp.getApp().getUserPrefs();
        if (!prefs.isLogined()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainRecyclerView.setAdapter(new DirAdapter());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (TextUtils.equals(prefs.getUserProfile().getUserEmail(), "admin@super.me")) {
            getMenuInflater().inflate(R.menu.admin_main, menu);
        } else {
            getMenuInflater().inflate(R.menu.user_main, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_user:
                startActivity(new Intent(this, RegistrationActivity.class));
                return true;
            case R.id.menu_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.label_logout_from_profile);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        prefs.resetPrefs();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    }
                });
                builder.setNegativeButton(android.R.string.no, null);
                builder.create().show();
            default:
                return super.

                        onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        scheduleNextNotificaton();
    }

    private Handler handler = new Handler(Looper.getMainLooper());

    public void scheduleNextNotificaton() {
        handler.removeCallbacks(notificationRunnable);
        handler.postDelayed(notificationRunnable, DateUtils.MINUTE_IN_MILLIS);
    }


    private Runnable notificationRunnable = new Runnable() {
        @Override
        public void run() {
            Bundle args = new Bundle();
            args.putBoolean(AppKeys.KEY_EXTRAS1, false);
            DialogFragment dialogFragment = new CapchaDialogFragment();
            dialogFragment.setArguments(args);
            dialogFragment.show(getSupportFragmentManager(), "CAPCHA_DIALOG");
        }
    };

    private class DirAdapter extends RecyclerView.Adapter<DirectoryHolder> {

        private LayoutInflater inflater;

        public DirAdapter() {
            inflater = LayoutInflater.from(MainActivity.this);
        }

        @Override
        public DirectoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new DirectoryHolder(inflater.inflate(R.layout.listitem_directory, parent, false));
        }

        @Override
        public void onBindViewHolder(DirectoryHolder holder, int position) {
            boolean hasPermission = false;
            String name = String.valueOf(position + 1);
            if (TextUtils.equals(prefs.getUserProfile().getUserEmail(), "admin@super.me")) {
                hasPermission = true;
            } else if (prefs.getUserProfile().getEnabledPath().contains(name)) {
                hasPermission = true;
            }
            holder.init("Directory #" + name, (int) (Math.random() * 512) + 1, hasPermission);
        }

        @Override
        public int getItemCount() {
            return 40;
        }
    }
}
