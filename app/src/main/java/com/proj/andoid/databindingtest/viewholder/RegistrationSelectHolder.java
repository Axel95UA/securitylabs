package com.proj.andoid.databindingtest.viewholder;

import android.content.Intent;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.proj.andoid.databindingtest.R;
import com.proj.andoid.databindingtest.constants.AppActions;
import com.proj.andoid.databindingtest.constants.AppKeys;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * created by Alex Ivanov on 2/22/16.
 */
public class RegistrationSelectHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.dirName)
    protected TextView dirName;
    @Bind(R.id.dirChecked)
    protected AppCompatCheckBox dirCheck;

    public RegistrationSelectHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    private String name;

    public void init(String name, boolean checked) {
        this.name = name;
        dirName.setText("Directory #" + name);
        dirCheck.setChecked(checked);
    }

    @OnClick(R.id.dirChecked)
    @SuppressWarnings("unused")
    protected void onDirChecked() {
        itemView.getContext().sendBroadcast(new Intent(AppActions.ACTION_DIR_SELECTED).putExtra(AppKeys.KEY_EXTRAS1, name));
    }
}
