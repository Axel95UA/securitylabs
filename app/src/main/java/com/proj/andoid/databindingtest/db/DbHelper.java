package com.proj.andoid.databindingtest.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * created by Alex Ivanov on 2/15/16.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final int INITIAL_DATABASE_VERSION = 1;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "appData";

    public interface Tables {
        String USERS = "usersTable";
        String LOG = "logs";
    }

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createUsersTable(db);
        createLogTable(db);
        onUpgrade(db, INITIAL_DATABASE_VERSION, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1) {
            // Update from 1 to 2 in the future
        }
        db.setVersion(newVersion);
    }

    private void createUsersTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.USERS + " (" +
                // BaseColumns
                BaseColumns._ID + " INTEGER PRIMARY KEY" +
                // Pending messages columns
                "," + DbContract.UserFields.email + " TEXT NOT NULL" +
                "," + DbContract.UserFields.password + " TEXT NOT NULL" +
                "," + DbContract.UserFields.enabledPath + " TEXT NOT NULL);");
    }

    private void createLogTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE" + Tables.LOG + " (" +
                        BaseColumns._ID + "INTEGER PRIMARY KEY" +
                        "," + DbContract.LogFields.userEmail + " TEXT NOT NULL" +
                        "," + DbContract.LogFields.info + " TEXT NOT NULL" +
                        "," + DbContract.LogFields.timestamp + " INTEGER NOT NULL);"
        );
    }
}
