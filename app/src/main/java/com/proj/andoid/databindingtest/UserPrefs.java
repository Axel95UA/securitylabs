package com.proj.andoid.databindingtest;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.proj.andoid.databindingtest.model.User;

/**
 * created by Alex Ivanov on 2/21/16.
 */
public class UserPrefs {

    private static UserPrefs prefs;
    private SharedPreferences userPreferences;

    public static UserPrefs getInstance() {
        if (prefs == null) {
            prefs = new UserPrefs();
        }
        return prefs;
    }

    public UserPrefs() {
        userPreferences = PreferenceManager.getDefaultSharedPreferences(SecureApp.getApp());
    }

    public void setUserProgile(User user) {
        userPreferences.edit()
                .putInt(Fields.KEY_USER_ID, user.getId())
                .putString(Fields.KEY_USER_EMAIL, user.getUserEmail())
                .putString(Fields.KEY_USER_PASSWORD, user.getPassword())
                .putString(Fields.KEY_USER_PATH, user.getPathAsString())
                .apply();
    }

    public void setLogined() {
        userPreferences.edit().putBoolean(Fields.KEY_LOGIN, true).apply();
    }

    public boolean isLogined() {
        return userPreferences.getBoolean(Fields.KEY_LOGIN, false);
    }

    public User getUserProfile() {
        User user = new User();
        user.setId(userPreferences.getInt(Fields.KEY_USER_ID, 0));
        user.setUserEmail(userPreferences.getString(Fields.KEY_USER_EMAIL, ""));
        user.setPassword(userPreferences.getString(Fields.KEY_USER_PASSWORD, ""));
        user.parsePathFromString(userPreferences.getString(Fields.KEY_USER_PATH, ""));
        if (TextUtils.isEmpty(user.getUserEmail()) || TextUtils.isEmpty(user.getPassword())) {
            return null;
        }
        return user;
    }

    public void resetPrefs() {
        userPreferences.edit().clear().apply();
    }

    private interface Fields {
        String KEY_LOGIN = "user.key.login";
        String KEY_USER_ID = "user.key.id";
        String KEY_USER_EMAIL = "user.key.email";
        String KEY_USER_PASSWORD = "user.key.pass";
        String KEY_USER_PATH = "user.key.path";
    }
}
